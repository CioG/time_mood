import { createStore } from 'vuex'
var moment = require("moment");
export default createStore({

  state: {

    accesos: [],
    acceso: {
      id: "",
      clave: "",
      contrasena: "",
      date: ""
    }
  },
  mutations: {
    cargar(state, payload) {
      state.accesosreg = payload
    },

    set(state, payload) {
      state.accesos.push(payload)
      localStorage.setItem('accesoreg', JSON.stringify(state.accesos))
      console.log("empujo la tarea", state.accesos);
    }
  },
  actions: {
    //La accion recibe ese acceso y relaizamos nuestro commit, ese acceso viaja dentro de nuestro commit de mutations, dentro de nuestro array accesos, con la funcion push.
    setAccesos({ commit }, acceso) {
      commit('set', acceso)
    },

    cargarLocalStorage({ commit }) {
      if (localStorage.getItem('accesos')) {
        const accesosreg = JSON.parse(localStorage.getItem('accesosreg'))
        commit('cargar', accesosreg)
        return
      }
      //Si no existe creamos este accesosreg.:
      localStorage.setItem('accesosreg', JSON.stringify([]))

    }
  },
  modules: {
  }
})
